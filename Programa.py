#Importar la clase
from Calculadora import Calculadora

class Interactuador:

    def menu_principal(self):
        print("Bienvenido a esta farsa de Calculadora")
        print("1. sumar")
        print("2. restar")
        print("3. multiplicar")
        print("4. dividir")
        print("5. módulo")
        entrada= int(input("Ingrese la opción:"))
        return entrada

    def ingresar_datos(self):
        x=int(input("Ingrese primer número:"))
        y=int(input("Ingrese segundo número:"))
        return x,y

    def programa(self):
        opcion=self.menu_principal()
        numero1, numero2= self.ingresar_datos()
        #Crear un objeto de la clase Calculadora
        miCalculadora=Calculadora()
        miCalculadora.operando_1=numero1
        miCalculadora.operando_2=numero2

        if opcion==1:
            miCalculadora.sumar()
        elif opcion==2:
            miCalculadora.restar()
        elif opcion==3:
            miCalculadora.multiplicar()
        elif opcion==4:
            miCalculadora.dividir()
        elif opcion==5:
            miCalculadora.modulo()

        print(miCalculadora.resultado)


# Crear un objeto de la clase interactuador

david= Interactuador()

david.programa()




